Running for development purposes:

* Compile FOCR from FOCR Resurrection: https://gitlab.com/idlersc/focr-resurrection
* Place `focr` in the same directory as GOFOCR. Add `+x` permissions, if necessary.
* Run `run.sh`

===

Running a production server:

* Run `compile.sh`
* Create a standalone user to run GOFOCR.
* Install, enable and run the included service via systemd.
