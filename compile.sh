#!/bin/sh
go build main.go
echo "(If running on root port) Now run this as root: /usr/sbin/setcap CAP_NET_BIND_SERVICE=+eip /path/to/exec"
