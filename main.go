package main

import (
	"html/template"
	"log"
	"net/http"
	"regexp"
	"os"
	"io"
	"github.com/google/uuid"
	"os/exec"
//	"strings"
//	"time"
)

type Page struct {
	Body      string
}

func captchaHandler(w http.ResponseWriter, r *http.Request, title string) {

	log.Printf("%s received CAPTCHA request", r.RemoteAddr)
	file, fileHeader, err := r.FormFile("fileupload")
	defer file.Close()

	if err != nil {
		log.Printf("%s did not send a captcha it seems", r.RemoteAddr)
		http.Error(w, "invalid file", http.StatusInternalServerError)
		return
	}

	fileSize := fileHeader.Size
	log.Printf("%s sent filesize %d", r.RemoteAddr, fileSize)

	if fileSize > 2048 {
		log.Printf("%s sent too large of a captcha it seems", r.RemoteAddr)
		http.Error(w, "file too large", http.StatusInternalServerError)
		return
	}

	newUuid := uuid.New()
	fileName := "./captchas/" + newUuid.String() + ".bmp"

	f, err := os.OpenFile(fileName, os.O_WRONLY | os.O_CREATE, 0666)
	defer f.Close()
	io.Copy(f, file)

	content, err := exec.Command("./focr", fileName).CombinedOutput()

	if err != nil {
		log.Printf("%s error solving captcha: %s", r.RemoteAddr, fileName)
		http.Error(w, "error solving captcha", http.StatusInternalServerError)
		return
	}

	log.Printf("%s %s captcha is %s", r.RemoteAddr, fileName, content)

	p := &Page{Body: string(content)}
	renderTemplate(w, "captcha", p)

}

var templates = template.Must(template.ParseFiles("templates/captcha.html"))

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		log.Printf("error processing request: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

var validPath = regexp.MustCompile("^(/captcha(.*))$")

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			log.Printf("%s requested and received 404", r.RemoteAddr)
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

func main() {
	log.Printf("Setting up handlers...\n")
	http.HandleFunc("/captcha", makeHandler(captchaHandler))

	log.Printf("Starting server...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
